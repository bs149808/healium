function Healium_CreateMiniMapButton()
	local db = {
		profile = {
			minimap = { 
				hide = false, 
			}, 
		},
	}
	local libIcon = LibStub("LibDBIcon-1.0")
	local libBroker = LibStub:GetLibrary("LibDataBroker-1.1"):NewDataObject("HealiumNim", {
		type = "launcher",
		icon = "Interface\\Icons\\Spell_Holy_LayOnHands",
		OnClick = function(frame, button)
			if (IsControlKeyDown()) then
				Healium_ToggleAllFrames()
			else
				Lib_ToggleDropDownMenu(1, nil, HealiumMenu, frame, 0, 0)
			end
		end,
		OnEnter = function(frame)
			GameTooltip:SetOwner(frame, "ANCHOR_BOTTOMLEFT", 32, 0)
			GameTooltip:SetText(Healium_AddonColor .. Healium_AddonName .. "|r |n|cFF55FF55Left Mouse |cFFFFFFFF" .. Healium_AddonName .. " Menu|n|cFF55FF55Right Mouse |cFFFFFFFFMove Button|n|cFF55FF55Ctrl & Left Mouse |cFFFFFFFF Toggle Frames")
			GameTooltip:Show()
		end,
		OnLeave = function(frame)
			GameTooltip:Hide()
		end,
	})
	libIcon:Register("HealiumNim", libBroker, db.profile.minimap)
end
